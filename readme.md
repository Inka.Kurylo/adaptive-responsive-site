Our team consists of 2 members (Vitalina_student 1 and Inna_Student 2)

Our tasks' scope:
Vitalina

1. Made variables for the project
2. Add components' directory
3. Made header section
4. Made testimonials' section
5. Positioned download button in header section and added hover
6. Made a rem transforming sass function

Inna

1. Set up a dev environment (gulpfile, npm plagins, directories, basic project architecture, files)
2. Created a Git repo
3. Made a section about editor
4. Made a pricing section
